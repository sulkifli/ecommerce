<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
        $administrator->username = "tugasecommerce";
        $administrator->name = "admin ecommerce";
        $administrator->phone = "08534204020";
        $administrator->email = "tugasecommerce@email.com";
        $administrator->roles = json_encode(["ADMIN"]);
        $administrator->password = \Hash::make("12345");
        $administrator->avatar = "belum memiliki gambar";
        $administrator->address = "adiyaksa baru no 1";

        $administrator->save();

        $this->command->info("User Admin Berhasil di insert");
    }
}
