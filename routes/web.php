<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('Auth.login');
});

Route::match(['GET', 'POST'], '/register', function () {
    return redirect("/login");
})->name("register");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');

Route::delete('categories/{id}/deletepermanent', 'CategoryController@deletepermanent')->name('categories.delete-permanent');
Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::resource('categories', 'CategoryController');

Route::get('books/caritrash', 'BookController@caritrash')->name('caritrash');
Route::delete('books/{id}/delete-permanent', 'BookController@deletepermanent')->name('books.delete-permanent');
Route::post('books/{book}/restore', 'BookController@restore')->name('books.restore');
Route::get('books/trash', 'BookController@trash')->name('books.trash');
Route::resource('books', 'BookController');
Route::get('/ajax/categories/search', 'CategoryController@ajaxSearch');

Route::resource('orders', 'OrderController');
Route::get('orders/ajax/{id?}', 'OrderController@ajax');
