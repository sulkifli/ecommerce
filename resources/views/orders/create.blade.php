@extends('layouts.global')
@section('title') Create Order @endsection

@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-
rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-
rc.0/js/select2.min.js"></script>

<script>
    $(".D").click(function() {
//    let id = $(".D").val();
   let id =$(this).find(':selected').data('id')
       $.get("http://localhost:8000/orders/ajax/"+id, function(data){
        $(".PRICES").val(data.price)
       })
       
   });
    </script>

@endsection
@section('content')
 <div class="row">
 <div class="col-md-8">
    <form action="{{route('orders.store')}}"method="POST"
    enctype="multipart/form-data"
    class="shadow-sm p-3 bg-white">
    @csrf
    <label for="number">Invoice Number</label><br>
    <input type="number" class="form-control" id="stock" name="invoice_number"
   min=0 value=0>
   <br>
   <label for="judul_Book">Judul Book</label><br>
<select class="form-control D" name="judul_book" id="judul_Book">
    @foreach ($buku as $bukus)
<option value="{{$bukus->title}}" data-id="{{$bukus->id}}" class="buku">{{$bukus->title}}</option>

   
    @endforeach
</select>
<br>
<label for="total_price">price</label> <br>

    <input type="text" class="form-control PRICES" name="total_price" id="total_price"
placeholder="total_price Orders" value="">


   <br>
    <label for="quantity">Quantity</label> <br>
    <input type="text" class="form-control" name="quantity" id="quantity"
   placeholder="quantity Orders">
   <br>
    <button
    class="btn btn-primary"
    name="save_action"
    value="SUBMIT">Publish</button>
    </form>
    </div>
</div>
    @endsection
 
    

  