<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $buyer_email = $request->get('buyer_email');
        $orders = \App\Order::with('user')->with('books')->whereHas('user', function ($query) use ($buyer_email) {
            $query->where('email', 'LIKE', "%$buyer_email%");
        })->where('status', 'LIKE', "%$status%")->paginate(3);

        return view('orders.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku = \App\Book::all();
        return view('orders.create', ['buku' => $buku]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $order_book = new \App\Order;
        $harga = $request->get('total_price');
        $quantitiy = $request->get('quantity');
        $order_book->quantity = $quantitiy;
        $order_book->total_price = $quantitiy * $harga;
        $order_book->judul_buku = $request->get('judul_book');
        $order_book->invoice_number = $request->get('invoice_number');
        $order_book->status = $request->get('save_action');
        $order_book->user_id = \Auth::user()->id;
        $order_book->save();
        return redirect()->route('orders.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = \App\Order::findOrFail($id);
        return view('orders.edit', ['order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = \App\Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->save();
        return redirect()->route('orders.edit', [$order->id])->with(
            'status',
            'Order status succesfully updated'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    // mendapatkan data price dengan mengirimkan id 
    public function ajax(Request $request, $id)
    {
        $data = Book::where('id', $id)->first();
        return response()->json($data);
    }
}
